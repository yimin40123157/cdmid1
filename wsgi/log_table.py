import math
print("<table border='1'>")
# 印對數表第一列
print("<tr><td>x</td>")
for j in range(10):
    print("<td>"+str(j)+"</td>")
print("</tr>")
# 印出各對應表內容
num = 11
for i in range(num):
    print("<tr>")
    ytitle = str(int((1+i*0.1)*10))
    # 印出每一列第一格
    print("<td>"+ytitle+"</td>")
    # 重複印對應格資料
    for j in range(10):
        y = 1+i*0.1+j*0.01
        ystring, xstring= str(round(y*10,2)).split(".")
        logvalue = int(round(math.log(y, 10)*10000, 0))
        print("<td>"+str(logvalue)+"</td>")
        #print(ystring, xstring, int(round(math.log(y, 10)*10000, 0)))
    #print("----")
    print("</tr>")
print("</table>")